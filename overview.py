import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import streamlit as st

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

st.title("Why did I moved from Notebooks to Streamlit")

st.text("")
st.text("")
if st.checkbox("Who am I?"):
    st.write(
        """
        Johan Heyns: Mechanical engineer looking for excuses to do math.
        
        * 2009-2011: Post-graduate studies: Modeling sloshing in coffee cups 
        (and fuel tanks)
        * 2012-2018: CFD at CSIR (Sloshing, High-speed aero, FreeCAD) and 
        started OpenSim 
        * 2018-2020: Dark years of data-science and credit modelling
        * 2020: At Ex Mente, I have slowed down and I heating up!
        
        """
    )
    st.set_option("deprecation.showImageFormat", False)
    col1, col2, col3 = st.beta_columns(3)
    with col3:
        st.image("logo.png", caption=" ", use_column_width=True, format="PNG")


st.text("")
st.text("")
if st.checkbox("Background"):
    st.write(
        """
        I am looking for a solution to quickly explorer and manipulate data. 
        My goal is not to display code, but to rather to give context and where 
        possible show some color full graphs.

        Notebooks was for very long my go-to!

        There are, however, a few things that *Johan Heyns* do not like about 
        Notebooks. 
        
        * Version control :disappointed:
        * In-line code execution (reminder of SAS)
        * Widgets (there but ugly)
        """
    )

st.text("")
st.text("")
if st.checkbox("Streamlit"):
    st.write(
        """
        Until 2019 ... and Streamlit was launched! The aim was to provide 
        tinkerers, engineers, and scientists with a simple framework 
        to craft beautiful tools.
        """
    )

    st.write(
        "<style>div.Widget.row-widget.stRadio > div{flex-direction:row;}</style>",
        unsafe_allow_html=True,
    )
    considerations = st.radio("", ("The good,", "The bad", "and the Ugly"))
    st.text("")

    if considerations == "The good,":
        st.write(
            """
            Few things *Johan Heyns* likes about Streamlit
            * Only Python code (version control and collaboration sorted);
            * Super simple to add interactivity;
            * Markdown for documentation or context;
            * Rich Python ecosystem (eg. plotly and scikit-learn)
            """
        )
    elif considerations == "The bad":
        st.write(
            """
            ... but lets manage expectations
            * By default hides code; 
            * Single theme and layout; 
            * Not the best for production; and
            * It is **linear**!

            The goal is not to create a full interactive web app, but I am 
            rather looking for a replacement where I can tinker and test ideas. 
            For Pythonist (not keen on JS) that want more flexability have a 
            look at [Plotly Dash](https://plotly.com/dash/).
            """
        )
    # elif considerations == "and the Queen":
    # st.write(
    #     """
    #     The Queen is the only person in the UK allowed to drive without a
    #     license.
    #     """
    # )
    # st.image("queen_driving.jpg", use_column_width=True)
    elif considerations == "and the Ugly":
        st.image("ugly.jpg", use_column_width=True)
        st.write(
            """
            One, Two, Three, Four, Five, Six. Six perfect number? 

            Or is it Eight ... PEP8?
            """
        )

st.text("")
st.text("")
if st.checkbox("How does it look"):
    st.write(
        """
        You are looking at ***Streamlit!***
        """
    )

    def format_coefs(coefs):
        equation_list = [f"{coef}x^{i}" for i, coef in enumerate(coefs)]
        equation = "$" + " + ".join(equation_list) + "$"

        replace_map = {"x^0": "", "x^1": "x", "+ -": "- "}
        for old, new in replace_map.items():
            equation = equation.replace(old, new)

        return equation

    st.write("")
    st.write(
        """
        Lets test different regressions using scikit-learn
    """
    )

    degree = st.selectbox(
        "Degree of polynomial",
        (1, 2, 3, 4),
    )

    st.write("The actual regression code snippet ...")
    with st.echo():
        import pandas as pd

        from sklearn.linear_model import LinearRegression
        from sklearn.preprocessing import PolynomialFeatures

        df = px.data.tips()
        X = df.total_bill.values.reshape(-1, 1)
        x_range = np.linspace(X.min(), X.max(), 100).reshape(-1, 1)

        poly = PolynomialFeatures(degree)
        poly.fit(X)
        X_poly = poly.transform(X)
        x_range_poly = poly.transform(x_range)
        model = LinearRegression(fit_intercept=False)
        model.fit(X_poly, df.tip)
        y_poly = model.predict(x_range_poly)

    fig = px.scatter(df, x="total_bill", y="tip", opacity=0.65)

    equation = format_coefs(model.coef_.round(2))
    fig.add_traces(go.Scatter(x=x_range.squeeze(), y=y_poly, name=equation))

    st.write("")
    st.write("... as well as the regression.")
    st.write(fig)


st.text("")
st.text("")
if st.checkbox("Example"):
    st.write(
        """ I would like to take you through a simple example using Google 
    analytics data for the openSim website. 
    """
    )
